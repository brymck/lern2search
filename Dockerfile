FROM alpine

COPY ./gopath/bin/frontend /frontend

COPY ./frontend/static
COPY ./frontend/templates

RUN apk update && apk add ca-certificates wget && update-ca-certificates