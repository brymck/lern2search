all:
	$(MAKE) -C backend

proto: proto/helloworld.pb.go
	$(MAKE) -C frontend

proto/helloworld.pb.go: proto/helloworld.proto
	protoc $^ --go_out=plugins=grpc:.

.PHONY: all proto
