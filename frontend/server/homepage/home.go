package homepage

import (
	"log"
	"net/http"
	"time"
)

type Handlers struct {
	logger *log.Logger
}

func (h *Handlers) Logger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		startTime := time.Now()
		defer h.logger.Printf("request processed in %s\n", time.Now().Sub(startTime))
		next.ServeHTTP(w, r)
	})
}

func (h *Handlers) SetupRoutes(mux *http.ServeMux) {
	mux.Handle("/", h.Logger(http.FileServer(http.Dir("./static"))))
}

func NewHandlers(logger *log.Logger) *Handlers {
	return &Handlers{
		logger: logger,
	}
}
