lern2search
===========

Prerequisites
-------------

* [Docker](https://docs.docker.com/install/)
* [Node](https://nodejs.org/en/download/)
* [Python 3](https://www.python.org/downloads/)

Things we need
--------------

* Parse ticker data
* Load it into a database
* Search
* Learn from search
* Provide a front-end

Creating a front-end
--------------------

The first thing we're going to do is create a front-end:

```sh
npx create-react-app frontend
```
